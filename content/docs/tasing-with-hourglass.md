# TASing with Hourglass
This guide exists to teach you how to set up and get a reproducible sync with Hourglass, a TASing program for windows programs, on Windows XP.

## Prerequisites
There's a few things you're going to need in order to begin TASing DT3, and most of them are easy to come by.

### Hourglass

[Hourglass](http://tasvideos.org/EmulatorResources/Hourglass.html) is a TASing program designed to TAS windows programs, but it has a ton of problems:

* It only works properly on an actual computer running Windows XP
* It doesn't support mouse input at all
* It randomly crashes whenever you save states
    * It doesn't actually save the movie file until you save a state or click "stop running" which means that it can take ages to skip back to where you were and continue TASing
* Savestates are invalidated once you stop running or the emulator crashes due to trying to save your state

If you still want to try TASing despite all of those problems, you can get hourglass from the following location: https://github.com/TASVideos/hourglass-win32/releases

I have only tested r81 and r83; both have their own problems but they work for the most part.

### Windows XP

As mentioned earlier you're going to need to install Windows XP on a computer somewhere. It doesn't matter if it's a spare computer or just as a separate disk you boot into on your main computer.

I've tested hourglass/DT3/winXP inside of virtualbox and I couldn't get the game to get past the green loading progress bar at the very start. I doubt it would sync regardless so really i'd just suggest finding an actual computer to install it on.

Activating winXP is a little annoying nowadays and you'll generally have to use a crack, but most of the cracks out there are malware. Good luck.

A WinXP iso is somewhat difficult to find if you don't know where to look since as far as i'm aware it's not even possible to buy it anywhere. In general I would recommend getting an MSDN download from [the-eye](http://the-eye.eu/public/MSDN/Windows%20XP/) although you'll need to find a key/crack on your own.

### Distorted Travesty 3

Obviously you'll need a copy of the game as well. 

I recommend downloading the latest version from [the website](http://distortedtravesty.blogspot.com/p/spoiler-warning-this-page-contains.html), but I have a ton of other versions backed up if anybody wants them. The game hasn't been updated in a while and likely won't be updated again.

### (Optional) An SSD

If you're using Hourglass r83 then it's changed things around to store the savestates to disk instead of keeping everything in memory. For DT3 each savestate is 601MB which can take quite a long time to save/load depending on your CPU and whether or not you're using an SSD.

However since you'll be re-writing to the SSD a lot over the course of the TAS you'll likely lower its lifespan by quite a lot so you'll have to decide for yourself if it is really worth probably buying an SSD specifically for TASing DT3.

## Setting up Hourglass

Now assuming that you have installed Windows XP on some computer and transferred DT3 and hourglass to it (if you haven't, do so), you're now ready to set up the program. Upon running Hourglass you should be greeted with an interface that looks like the screenshot below:

![screenshot of hourglass on first boot](bluh.png)

1The first step is to select the executable at the bottom of the window by clicking on "Browse"; navigate to where the DT3 executable is and select it. After that you'll need to create a .WTF file which is done by clicking on `File` at the top left of the window and selecting `Record New Movie...`; Choose a file location along with a name for it, and select it. The game will then start up, but we need to set up a few settings first, so click "Stop Running" at the bottom of the window.

The first main setting you need to change is under `Runtime` at the top of the window. If `Message Sync Mode` is *not* set to `Unchecked (native messaging)` then you will be unable to input any keys in order to control the game once it is started up.

It might also be a good idea to go into the `Input` menu and change `Enable Game Input When`/`Enable Hotkeys When` to be focused at all times, as I would occasionally have problems with the focus being picked up if I didn't set them all; you're welcome to try other combinations to see what works for you and your setup. You can also set up your hotkeys here or simply look at them so that you know what they are.

A few other important settings are:

* Ensuring `Frames per Second` in the middle-left of the screen is set to 30 (unless you're using the 2x FPS cheat code).
* Setting `System Time` to whatever you want it to be for RNG manipulation. The default of 6000 is what i've generally had it set to but perhaps there's a more beneficial setting that will be found in the future. As far as i'm aware RNG is recalculated every frame so it is a little hard to manipulate it, but i've managed to do it to at least some extent for enemy drops.

All of the other default settings should be fine, so from here you can go back to `File` and choose `Save Config As...` to save out a config file. Hourglass doesn't store the config with the WTF file so you'll need to either change the settings every time you open it up again, or choose `Load Config From...` in the `File` menu.

## Starting up DT3

Now that we've done a small amount of basic setup for Hourglass we're now almost ready to start up DT3.

Near the bottom left of the window there is a somewhat odd setting labeled `Multithreading and Wait Sync`; By default it is set to `Wrap` although it can also be set to `Allow`. If you set the setting to `Disable` then no sound will play at all. All of the settings can properly sync and I haven't noticed much of a difference between the three of them other than the lack of sound on Disable, but maybe Allow/Wrap cause the game to crash more. If a file is made with a specific setting then it will require that level when playing back the movie otherwise it will desync. It's also possible to change the setting to disable at any point in time while running if it was set to something else beforehand, which i've utilized in one test WTF.

For some reason hourglass has the game unpaused by default when you start it up, so before you actually begin you need to make sure `Paused` is checked on the right side of the hourglass window. After you've done so you can now click on `Run and Record New Movie` to start up DT3 for the first time. You'll be greeted with the great green gamemaker8.1 loading window.

![Screenshot of gamemaker8.1 loading window](bluh.png)

At this point you might be a little confused since the gamemaker loading window will continue to load despite being "Paused". For some reason gamemaker will continue to just do its thing up until it finally loads into the main window at Frame 3. I'm not entirely sure why this is but it helps in our situation as it's a good way to ensure everything remains synced without worrying about load times across computers. If you start up DT3 without having `Paused` checked then it will always become desynced which is why I made sure to mention it in the previous paragraph.

## Resuming a TAS

After a while you'll come to a point where you need to continue on from where you left off previously, either due to needing to go to sleep, or probably because it crashed. 

When you open up the hourglass window again it should automatically open up the .WTF and executable you last had open, but most of the other settings will be reset back to the defaults. If you saved out a config file this would be a good time to load it, otherwise manually go through and change the few options you need to.

After you've made sure that `Paused` is checked you can then click on `Run and Play Existing Movie` and wait for the green gamemaker loader to finish.

If you have already TAS'd a large amount it might be a good idea to unpause it and check fast-forward until you get near the end (or use the hotkeys). If you're still near the beginning then maybe stepping through using frame advance or simply unchecking pause until you get closer is ideal.

Once you're at the end of your TAS you can go to the `File` menu and click on `Resume Recording from Now` in order for hourglass to actually pick up your inputs from now on and start writing into the same WTF file. Alternatively you could save into a separate file using `Resume Recording to Different File` in case you want to branch out and try something else from a specific point in time without going through the rather buggy savestates.

## Splicing a Movie File

It doesn't work. I've tried everything I could and nothing syncs.

**Do it right the first time or re-do everything after that point again if you find a mistake or improvement.**

\:realcry\:

## Next steps

From here the game should properly sync up as long as you do the few steps outlined above whenever you start up the game.

Below are a few recommended pages you can visit depending on what you want to do:

* [Any% Route]({{< relref "/docs/any_percent/01_intro.md" >}})
* [100% Route]({{< relref "/docs/100_percent/01_intro.md" >}})