---
---

# Intro

Assuming you're on the first frame the main window appears after the gamemaker8.1 loading progress bar, `Loading sounds` and another progress bar should be on the screen. If you're using hourglass this should be on frame 3.

After this very first frame you'll want to frame advance until the loading bar is off the screen and all you're left with is the blank title screen. This should be `Frames: 7 / 6` as this is the first possible frame that your input actually works. Then hold down J, frame advance, let go of J, and frame advance again. At this point you'll have skipped the intro sequence where the title slowly materialized and the main menu should now be fully present. You can now hold J, frame advance, let go of J, frame advance, to start a new game.

As you can tell DT3/gamemaker8.1 doesn't really process inputs for menus until you actually release a key so a lot of actions require two frames to do. I've even had issues with the pause menu requiring 1 frame down 2 frames up in order to not desync although I haven't had a desync happen in a while.

At this point you should be at a black screen which is the intro cutscene. There's basically a frame perfect skip directly after entering a new game where you: hold m, frame advance, let go of m, frame advance. If you wait an additional frame before pressing down M then you might be able to still bypass the screen but if you wait too long then you'll need to wait for the cutscene to fade in before you can skip it. For some reason fadeins/fadeouts are completely unskippable in the engine.

You should now be in the [1] stage slowly fading in; you can't skip the fading in at all. The first time you press M is like just before jerry starts appearing on screen after it's fully faded in. Assuming you've done everything else as the fastest possible moment then when hourglass says `Frames 91/90` you can press/release M and you should be able to skip the cutscene. If you've done it correctly then jerry should appear on the ground. Do yet another M press and release and jerry will be slightly in the air again falling down marking the end of the cutscene.

## Pause menu

At this point you can now open the pause menu to change all of the settings to what you feel would be best for the TAS. 

One weird quirk with the pause menu is that the icon that shows you what you've selected is delayed by one frame of where you actually have selected; Look at the help text at the bottom of the screen to determine what you actually have selected.

Some good settings to set:

* Unreal guy on obviously
* Character/ability swap to hold+direction probably
* Instantly skip the death screen
* Turn off low health warning since it gets annoying
* Turn on message plink for the few unskippable cutscenes and other messages that pop up randomly because it's kami
* Most importantly, hats

After you're finished in the menu you'll just be sitting there for what feels like ages until the virus-shooting segment starts so feel free to press up/down frame perfectly as much as you want until then.

## Virus-Shooting

Virus-shooting is basically an autoscroller and the timer starts off at 1249 on screen. There's a cooldown on the machine and a limited amount of angles you can shoot at. As it is an autoscroller you're free to do whatever you feel like that would seem the most entertaining as you can't save any time no matter what you do.

There's a few areas that have weird buggy collision that might be fun to show off, or how you can't actually shoot some of the viruses approaching from the left side of the screen due to the angles not matching up, although you can shoot the blue one that approaches later on a single frame.

I'd also suggest trying to do a bunch of weird bounce shots that hit stuff that just barely appears on the top of the screen or just slightly off screen.

## The ability to actually move

Once you're done with virus-shooting you get two dialogue prompts that you need to use J in order to advance; M doesn't work. I'd suggest save-stating just before virus-popping ends so you can find the correct frame to skip them on.

At this point the game starts to open up and you're free to do a lot of optimization, but most of the screens in the tutorial area are on cycles which makes it difficult before we have the ability to jump. The main method of movement in this section, and a lot of the run to be honest, is [crouch-cancelling]({{< relref "/docs/techniques.md#crouch-cancelling" >}}). I'd suggest reading that documentation page on it to get a full idea of how to do it.

Once your hitbox touches the door you can press up on it to fade out and fade in on the new screen. Shortly after the fade in a cutscene will play, which you can skip with M. You'll likely have to savestate/load constantly until you find the first frame to skip it.

## Cycle based hell

Have fun frame advancing through a bunch of slow moving cycle-based platforms with only the ability to move in directions and look up.

## Getting the jump boots

Once you have the jump boots everything becomes vastly more complex.

(to be continued)