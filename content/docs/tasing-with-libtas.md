---
---

# TASing with libTAS

This guide teaches you how to set up and get a reproducible sync using libTAS on linux, utilizing a windowsXP installation inside of pcEM.

This is rather experimental at the moment but should eventually be the preferred way to go about doing things once it matures a little more.

## Prerequisites
There's a few things you're going to need in order to begin TASing DT3, and most of them are easy to come by.

## A linux computer

Assuming your normal day to day operating system is linux you should be good to go. 

If you're not normally using linux then you can theoretically install linux inside of a virtualmachine (virtualbox/vmware/etc) and as the main developer of libtas also used a VM of linux to develop libTAS originally. Although the performance of emulating an emulation will probably be garbage and I'd really just suggest installing linux regardless.

### libTAS

[libTAS can be found on github](https://github.com/clementgallet/libTAS), if you don't feel like compiling it yourself the author provides official releases.

### pcEM

pcEM is a old computer emulator, similar to dosbox. Tasvideos.org has a forked version of the emulator that syncs better with libtas as the [following github](https://github.com/TASVideos/pcem/).

At present the branch to use is `v15_235273a` but it will likely change in the future.

### WindowsXP

Activating winXP is a little annoying nowadays and you'll generally have to use a crack, but most of the cracks out there are malware. Good luck.

A WinXP iso is somewhat difficult to find if you don't know where to look since as far as i'm aware it's not even possible to buy it anywhere. In general I would recommend getting an MSDN download from [the-eye](http://the-eye.eu/public/MSDN/Windows%20XP/) although you'll need to find a key/crack on your own.

### Distorted Travesty 3

Obviously you'll need a copy of the game as well. 

I recommend downloading the latest version from [the website](http://distortedtravesty.blogspot.com/p/spoiler-warning-this-page-contains.html), but I have a ton of other versions backed up if anybody wants them. The game hasn't been updated in a while and likely won't be updated again.

## Setting up libTAS with pcEM

I'm still not really sure at the moment tbh. Need to toy with winXP/pcEM/libTAS at the moment and how everything works together.

---

??????