# Crouch Cancelling

Crouch-Cancelling is done by running and then pressing down for a single frame, releasing for a single frame, and just continuing to repeat holding/releasing every other frame. 

There's "slow crouch-cancelling" and "fast crouch-cancelling" and unless you have to wait for something (and want to mess around) you'll always want to fast crouch-cancel. 

Slow crouch-cancelling comes about when you're standing still and then hold a direction for 1 frame to start running, then immediately after that start crouch-cancelling.

Fast crouch-cancelling comes about when you reach the fastest movement speed, which is after 3 frames of running, and *then* starting to crouch-cancel.