---
title: Introduction
type: docs
---

# DT3 TAS Info

This site was created as a dumping ground for various info needed when making a [Tool Assisted Speedrun (TAS)](http://tasvideos.org/WelcomeToTASVideos.html) for [Distorted Travesty 3 (DT3)](http://distortedtravesty.blogspot.com/p/spoiler-warning-this-page-contains.html).

Browse the links on the sidebar for further information.

Most of the site is still a work in progress and I might completely change how it is generated in the future.