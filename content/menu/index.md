---
headless: true
---

- **Program Setup**
    - [Hourglass]({{< relref "/docs/tasing-with-hourglass.md" >}})
    - ~~[libTAS]({{< relref "/docs/tasing-with-libtas.md" >}})~~

- **Any% Route**
    - [Intro]({{< relref "/docs/any_percent/01_intro.md" >}})

- **100% Route**
    - [Intro]({{< relref "/docs/100_percent/01_intro.md" >}})

---

- [Techniques]({{< relref "/docs/techniques.md" >}})

---

- **Contributing**
    - [Contributing]({{< relref "/docs/contributing/contributing.md" >}})
    - [Tag Examples]({{< relref "/docs/contributing/shortcodes.md" >}})
